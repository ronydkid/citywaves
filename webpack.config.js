rules: [
	{
		use: [
			{
				loader: 'sass-loader',
				options: {
					sourceMap: true,
					sourceMapContents: false
				}
			}
		]
	}
];
