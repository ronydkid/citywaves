import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class Nav extends Component {
	state = {};
	render() {
		return (
			<nav className="main-navigation">
				<a href="/">
					<h3>
						C <span>City Waves</span>
					</h3>
				</a>
				<ul>
					<Link className="nav-link" to="/">
						Home
					</Link>
					<Link className="nav-link" to="/places">
						Places
					</Link>
				</ul>
			</nav>
		);
	}
}

export default Nav;
