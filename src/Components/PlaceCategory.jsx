import React, { Component, useState, useEffect } from 'react';
function Places() {
	useEffect(() => {
		fetchItems();
	}, []);
	const [ item, setItem ] = useState([]);
	const fetchItems = async () => {
		const data = await fetch('https://developers.zomato.com/api/v2.1/categories', {
			headers: { 'user-key': 'be3436d8432c2fe82aace1869a2897e3' }
		});
		const items = await data.json();
		console.log(items.categories);
		setItem(items.categories);
	};

	return (
		<div>
			<h1>Places</h1>
		</div>
	);
}

export default Places;
