import React from 'react';
import './App.css';
import Nav from './Components/Nav';
import Home from './Pages/Homepage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Places from './Pages/Places';
import NightLife from './Pages/NightLife';
import NightLifeDetails from './Pages/NightLifeDetails';

function App() {
	return (
		<Router>
			<div className="App">
				<Nav />
				<Switch>
					<Route path="/" exact component={Home} />
					<Route path="/places" component={Places} />
					<Route path="/nightlife" exact component={NightLife} />
					<Route path="/nightlife/club/:id" component={NightLifeDetails} />
				</Switch>
			</div>
		</Router>
	);
}

export default App;
