import React, { Component } from 'react';

class Homepage extends Component {
	state = {};
	render() {
		return (
			<section className="half-screen">
				<section
					className="first-half"
					style={{ backgroundImage: 'url(../images/empty-dining-tables-and-chairs-1579739.jpg)' }}
				>
					<div className="section-content">
						<p className="description">Check out the best restaurants, caffes and bars around you!</p>
						<a href="#" className="browse-locations default">
							Browse
						</a>
					</div>
				</section>
				<section
					className="second-half"
					style={{ backgroundImage: 'url(../images/lighted-dj-controller-2111015.jpg)' }}
				>
					<div className="section-content">
						<p className="description">Find out how the nightlife is around you!</p>
						<a href="/nightlife" className="browse-locations default">
							Browse
						</a>
					</div>
				</section>
			</section>
		);
	}
}
export default Homepage;
