import React, { Component, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
function Places() {
	useEffect(() => {
		fetchItems();
	}, []);
	const [ items, setItems ] = useState([]);
	const fetchItems = async () => {
		const data = await fetch('https://jsonplaceholder.typicode.com/posts');
		const items = await data.json();
		console.log(items);
		setItems(items);
	};

	return (
		<div>
			<h1>Places</h1>
			{items.map((item) => (
				<Link className="places-link" to={`/places/${item.id}`}>
					{item.title}
				</Link>
			))}
		</div>
	);
}

export default Places;
