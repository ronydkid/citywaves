import React, { Component } from 'react';
import data from '../Dummydata/night.json';
class NightLifeDetails extends Component {
	state = {
		places: data,
		displayed: false,
		step: 1
	};
	componentDidMount = () => {
		const match = this.state.places.find((place) => place.id === this.props.match.params.id);
		this.setState({ details: match });
	};

	renderRating = (rating) => {
		console.log(rating);
		const value = [ ...Array(rating).keys() ];
		return value.map((val) => <i key={val} className="far fa-star" />);
	};

	handleDisplay = (e) => {
		e.preventDefault();
		this.setState({ displayed: !this.state.displayed });
	};
	handleStepChange = (direction) => {
		const step = this.state.step;
		document.querySelector(`[step="${step}"]`).className = 'step';
		if (direction === 'next') {
			document.querySelector(`[step="${step + 1}"]`).className = 'step active';
			this.setState({ step: step + 1 });
		} else if (direction === 'previous') {
			document.querySelector(`[step="${step - 1}"]`).className = 'step active';

			this.setState({ step: step - 1 });
		}
	};
	render() {
		return (
			<div className="place-details">
				<br />
				{this.state && this.state.details ? (
					<div id="details">
						<div className="place-details-description">
							<h1 className="place-name">{this.state.details.name}</h1>
							<div className="slider-gallery">
								<div className="slider-image-container">
									<img
										src="https://images.pexels.com/photos/849/people-festival-party-dancing.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
										alt=""
									/>
								</div>
								<div className="slider-image-container">
									<img
										src="https://images.pexels.com/photos/736355/pexels-photo-736355.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
										alt=""
									/>
								</div>
								<div className="slider-image-container">
									<img
										src="https://images.pexels.com/photos/597056/concert-cheering-people-guitar-597056.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
										alt=""
									/>
								</div>
								<div className="slider-image-container">
									<img
										src="https://images.pexels.com/photos/759832/pexels-photo-759832.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
										alt=""
									/>
								</div>
								<div className="slider-image-container">
									<img
										src="https://images.pexels.com/photos/358129/pexels-photo-358129.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
										alt=""
									/>
								</div>
							</div>
							<p className="place-details">
								{this.state.details.description}
								Lorem, ipsum dolor sit amet consectetur adipisicing elit. Facilis obcaecati nam quam
								blanditiis ipsa. Nostrum repellendus, maxime beatae fugiat quia totam, corporis
								suscipit, illo sint omnis quasi ipsam sapiente nemo!
							</p>
							<br />
							<div className="start-end">
								<i className="far fa-clock" /> From 21:00 to 01:00
							</div>
							<div className="music">
								<h2 className="genre">Techno tuesday tonight!</h2>
								<h3 className="performer">DJ Mike Johson</h3>
							</div>
							<h2 className="details-promotions">just tonight: Vodka 10% off</h2>
						</div>
						<aside className="actions">
							<a href={`#`} className="browse-locations default" onClick={(e) => this.handleDisplay(e)}>
								Reservations
							</a>
							<a href={`#`} className="browse-locations default">
								Call us
							</a>
							<br />
							<span className="object-location-details">
								<i className="fas fa-thumbtack" />&nbsp;
								{this.state.details.location}
							</span>
							<br />
							{this.renderRating(this.state.details.rating)}
						</aside>
					</div>
				) : (
					''
				)}
				<div id="overflow-details" displayed={`${this.state.displayed}`}>
					<i className="fas fa-sign-out-alt close-reservations" onClick={this.handleDisplay} />
					<div className="step active" step="1">
						<h2>Pick a free table</h2>
						<div id="tables">
							<span className="table " />
							<span className="table" />
							<span className="table" />
							<span className="table taken" />
							<span className="table" />
							<span className="table" />
							<span className="table" />
							<span className="table" />
							<span className="table taken" />
							<span className="table" />
							<span className="table" />
							<span className="table" />
							<span className="table taken" />
							<span className="table taken" />
							<span className="table" />
							<span className="table" />
							<span className="table" />
							<span className="table" />
						</div>
					</div>
					<div className="step" step="2">
						<h2>this is step two</h2>
					</div>
					<i
						className="far fa-arrow-alt-circle-left reservations-previous"
						onClick={() => this.handleStepChange('previous')}
					/>
					<i
						className="far fa-arrow-alt-circle-right reservations-next"
						onClick={() => this.handleStepChange('next')}
					/>
				</div>
			</div>
		);
	}
}

export default NightLifeDetails;
