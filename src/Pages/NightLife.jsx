import React, { Component } from 'react';
import data from '../Dummydata/night.json';
import '../styles/stars.css';
class NightLife extends Component {
	state = {
		data: data
	};
	componentDidMount = () => {
		console.log(this.state.data);
	};

	renderRating = (rating) => {
		console.log(rating);
		const value = [ ...Array(rating).keys() ];
		return value.map((val) => <i key={val} className="far fa-star" />);
	};

	render() {
		return (
			<div id="night-life">
				<link
					href="https://fonts.googleapis.com/css?family=Lato:300,400,700"
					rel="stylesheet"
					type="text/css"
				/>
				<div id="stars" />
				<div id="stars2" />
				<div id="stars3" />

				{this.state &&
					this.state.data &&
					this.state.data.map((location) => (
						<div className="card" key={location.id} id={location.id}>
							<div className="image">
								<img src={location.image} alt="" />
							</div>
							{location.promotions ? <p className="object-promotions">{location.promotions}</p> : ''}

							<div className="card-content">
								<h2 className="object-name">{location.name}</h2>
								<p className="object-description">{location.description}</p>
								<span className="object-location">
									<i className="fas fa-thumbtack" />&nbsp;
									<i>{location.location}</i>
								</span>
								<br />
								<span className="object-rating">{this.renderRating(location.rating)}</span>
								<br />
								<a href={`/nightlife/club/${location.id}`} className="browse-locations default">
									Book
								</a>
							</div>
						</div>
					))}
			</div>
		);
	}
}

export default NightLife;
